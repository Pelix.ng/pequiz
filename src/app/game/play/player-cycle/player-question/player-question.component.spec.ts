import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerQuestionComponent } from './player-question.component';

describe('PlayerQuestionComponent', () => {
  let component: PlayerQuestionComponent;
  let fixture: ComponentFixture<PlayerQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
